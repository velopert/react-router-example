# React-Router-Example

```javascript
ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home}/>
      <Route path="home" component={Home}/>
      <Route path="introduce" component={Introduce}>
        <IndexRoute component={ServiceA}/>
        <Route path="servicea" component={ServiceA}/>
        <Route path="serviceb" component={ServiceB}/>
      </Route>
      <Route path="/another_route" component={AnotherRoute}/>
      <Route path="/another_route/:value" component={AnotherRoute}/>
    </Route>
  </Router>,
  document.getElementById('root')
);
```