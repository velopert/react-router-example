import React, {Component} from 'react';

class AnotherRoute extends Component {
    render() {
        return (
            <div>
                <h2>Another Route</h2>
                { this.props.params.value === undefined ? 'Try to put something at the back of the URL! e.g. localhost:3000/another_route/anything' : `You've entered "${this.props.params.value}"`}
            </div>
        );
    }
}

export default AnotherRoute;