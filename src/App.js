import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link } from 'react-router';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <div>
          <Link to={'/home'}><h3>Home</h3></Link>
          <Link to={'/introduce'}><h3>Introduce</h3></Link>
          <Link to={'/another_route'}><h3>Another Route</h3></Link>
        </div>
        <div className="App-intro">
          { this.props.children }
        </div>
      </div>
    );
  }
}

export default App;
