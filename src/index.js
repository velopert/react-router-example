import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Home from './Home';
import Introduce from './Introduce';
import ServiceA from './ServiceA';
import ServiceB from './ServiceB';
import AnotherRoute from './AnotherRoute';

import { IndexRoute, Router, Route, browserHistory } from 'react-router';

import './index.css';

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home}/>
      <Route path="home" component={Home}/>
      <Route path="introduce" component={Introduce}>
        <IndexRoute component={ServiceA}/>
        <Route path="servicea" component={ServiceA}/>
        <Route path="serviceb" component={ServiceB}/>
      </Route>
      <Route path="/another_route" component={AnotherRoute}/>
      <Route path="/another_route/:value" component={AnotherRoute}/>
    </Route>
  </Router>,
  document.getElementById('root')
);
