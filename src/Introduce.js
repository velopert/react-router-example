import React, {Component} from 'react';
import { Link } from 'react-router';

class Introduce extends Component {
    render() {
        return (
            <div>
                <h1>Introduce</h1>
                <div>
                    <Link to={'/introduce/servicea'}><h3>ServiceA</h3></Link>
                    <Link to={'/introduce/serviceb'}><h3>ServiceB</h3></Link>
                </div>
                <div>{this.props.children}</div>
            </div>
        );
    }
}

export default Introduce;